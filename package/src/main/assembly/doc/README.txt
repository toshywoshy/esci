OpenVPMS ESCI examples ${project.version} release
=============================================

1. Installation Notes

1.1 Requirements
- These examples require the following to be installed:

  - Java 8
    See https://www.oracle.com/java/technologies/javase-downloads.html

  - Tomcat 8.x or 9.x
    See https://tomcat.apache.org/download-80.cgi

1.2 Directory structure

 The example package has a single top-level directory named
 ${pom.artifactId}-${project.version}
 This will be referred to as <INSTALL_HOME> in the remainder of this document.
 This directory has the following sub-directories:

 - bin     scripts for running the client examples
 - conf    configuration files for the examples
 - data    sample data
 - lib     jars used by the examples in bin/
 - src     the source code for the examples
 - webapp  the example web application
 - doc     ESCI documentation
 - wsdl    WSDL for the ESCI services

1.3 Configure tomcat to use SSL

 See http://tomcat.apache.org/tomcat-8.0-doc/ssl-howto.html

1.4 Configure Tomcat users

 For these examples, a user needs to be added to tomcat's user database.
 The default tomcat installation stores user information in <TOMCAT_HOME>/conf/tomcat-users.xml
 Add the following entries:
    <role rolename="ESCI"/>
    <user username="esci" password="example" roles="ESCI"/>

 See http://tomcat.apache.org/tomcat-8.0-doc/realm-howto.html#UserDatabaseRealm
 for more details.

1.5. Example web application installation

  To install the example web application,
  copy <INSTALL_HOME>/webapp/esci-example.war
  to the <TOMCAT_HOME>/webapps directory.

2. Testing the service

  To test the installation, open up a web browser and enter the URL:

      https://localhost:8443/esci-example/OrderService?wsdl

  A dialog should pop up, requesting a user name and password.
  Enter 'esci' and 'example' respectively (without quotes).
  The WSDL for the OrderService should be displayed.

3. Submitting orders

  To submit an order, go the to <INSTALL_HOME>/bin directory and enter:

  > order.bat -f ../data/order1.xml -u esci -p example

  The service will log the order to the console.

4. Security considerations

  It is recommended that all ESCI traffic occurs over secure https connections.
  The samples are configured to use https connections by default.
  For testing purposes, it is often simpler to configure web services to use 
  http.
  This can be done by changing the web.xml file after the esci-example.war 
  file has been deployed to Tomcat.
  Tomcat extracts the .war to <TOMCAT_HOME>/webapps/esci-example
  
  Edit <TOMCAT_HOME>/webapps/esci-example/WEB-INF/web.xml and change
  transport-guarantee to NONE to enable requests to go over http.
    E.g:
     <user-data-constraint>
          <transport-guarantee>NONE</transport-guarantee>
     </user-data-constraint>

    The examples can now be run using http, on port 8080:
    > order.bat -f ../data/order1.xml -r http://localhost:8080/esci-example/RegistryService -u esci -p example

  NOTE: under this scenario, user credentials are submitted using BASIC authentication, which is insecure.
  BASIC authentication is OK for https, as the transport itself is encrypted.

  While this release of ESCI supports DIGEST authentication, support for it will be dropped in later releases.
