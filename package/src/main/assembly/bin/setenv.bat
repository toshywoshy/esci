rem set JAVA_OPTS=-Dcom.sun.xml.ws.transport.http.client.HttpTransportPipe.dump=true

set JAVA_OPTS=%JAVA_OPTS% -Djavax.net.ssl.trustStore=../conf/tomcat.jks -Djavax.net.ssl.keyStore=../conf/tomcat.jks -Djavax.net.ssl.trustStorePassword=changeit -Djavax.net.ssl.keyStorePassword=changeit
set CLASSPATH=..\conf

FOR %%J IN (..\lib\*.jar) DO call :addcp %%J
goto gotcp

:addcp
set CLASSPATH=%CLASSPATH%;%1
goto :eof

:gotcp
