/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.service;

import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.basic.DocumentTypeType;
import org.openvpms.esci.ubl.common.basic.IDType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.io.UBLDocumentWriter;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;


/**
 * Base class for sample services.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public abstract class SampleService {

    /**
     * UBL document context for displaying received UBL documents.
     */
    private final UBLDocumentContext context;

    /**
     * Default constructor.
     *
     * @throws javax.xml.bind.JAXBException if the document context cannot be created
     * @throws org.xml.sax.SAXException     if the document context cannot be created
     */
    public SampleService() throws JAXBException, SAXException {
        context = new UBLDocumentContext();
    }

    /**
     * Logs an object.
     *
     * @param object the object to log
     */
    protected void log(Object object) {
        UBLDocumentWriter writer = context.createWriter();
        writer.setFormat(true);
        try {
            writer.write(object, System.out, false);
        } catch (JAXBException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Helper to create a document reference.
     *
     * @param type the document type
     * @param id   the identifier
     * @return a new document reference
     */
    protected DocumentReferenceType createDocumentReference(String type, IDType id) {
        DocumentReferenceType ref = new DocumentReferenceType();
        DocumentTypeType docType = new DocumentTypeType();
        docType.setValue(type);
        ref.setDocumentType(docType);
        ref.setID(id);
        return ref;
    }

}
