/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.example.service;

import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.basic.IDType;
import org.openvpms.esci.ubl.invoice.InvoiceType;
import org.openvpms.esci.ubl.order.OrderResponseSimpleType;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.xml.bind.JAXBException;


/**
 * Posts documents to the {@link SampleInboxService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class SamplePostService extends SampleService implements PostService {

    /**
     * The inbox.
     */
    private SampleInboxService inbox;

    /**
     * Default constructor.
     *
     * @throws JAXBException if the document context cannot be created
     * @throws SAXException  if the document context cannot be created
     */
    public SamplePostService() throws JAXBException, SAXException {
    }

    /**
     * Post a document to the inbox service.
     *
     * @param document the document to post
     */
    @Override
    public void post(Object document) {
        log(document);
        DocumentReferenceType ref;
        if (document instanceof OrderResponseSimpleType) {
            ref = createDocumentReference("OrderResponseSimple", ((OrderResponseSimpleType) document).getID());
        } else if (document instanceof InvoiceType) {
            ref = createDocumentReference("Invoice", ((InvoiceType) document).getID());
            inbox.addDocument(ref, document);
        } else {
            IDType id = new IDType();
            id.setValue("-1");
            ref = createDocumentReference("unknown", id);
        }
        inbox.addDocument(ref, document);
    }

    /**
     * Registers the inbox.
     *
     * @param inbox the inbox
     */
    @Resource
    public void setInbox(SampleInboxService inbox) {
        this.inbox = inbox;
    }
}