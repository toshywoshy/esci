/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.service;

import org.openvpms.esci.service.InboxService;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Sample inbox service.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class SampleInboxService extends SampleService implements InboxService {

    /**
     * The documents in the inbox.
     */
    private Map<DocumentReferenceType, Object> docs
            = Collections.synchronizedMap(new LinkedHashMap<DocumentReferenceType, Object>());

    /**
     * Default constructor.
     *
     * @throws javax.xml.bind.JAXBException if the document context cannot be created
     * @throws org.xml.sax.SAXException     if the document context cannot be created
     */
    public SampleInboxService() throws JAXBException, SAXException {
    }

    /**
     * Returns a list of document references, in the order that they were received.
     * <p/>
     * Each reference uniquely identifies a single document.
     *
     * @return a list of document references
     */
    @Override
    public List<DocumentReferenceType> getDocuments() {
        return new ArrayList<DocumentReferenceType>(docs.keySet());
    }

    /**
     * Returns the document with the specified reference.
     *
     * @param reference the document reference
     * @return the corresponding document, or <tt>null</tt> if the document is not found
     */
    @Override
    public Document getDocument(DocumentReferenceType reference) {
        Document doc = null;
        Object result = docs.get(find(reference));
        if (result != null) {
            log(result);
            doc = new Document();
            doc.setAny(result);
        }
        return doc;
    }

    /**
     * Acknowledges a document.
     * <p/>
     * Once acknowledged, the document will no longer be returned by {@link #getDocuments} nor {@link #getDocument}.
     *
     * @param reference the document reference
     */
    @Override
    public void acknowledge(DocumentReferenceType reference) {
        docs.remove(find(reference));
    }

    /**
     * Adds a document to the inbox.
     *
     * @param reference the document reference
     * @param document  the document to add
     */
    public void addDocument(DocumentReferenceType reference, Object document) {
        docs.put(reference, document);
    }

    /**
     * Finds a document reference.
     * <p/>
     * This matches on id and document type.
     *
     * @param reference the reference to find
     * @return the corresponding reference, or <tt>null</tt> if none is found
     */
    private DocumentReferenceType find(DocumentReferenceType reference) {
        String id = reference.getID().getValue();
        String type = reference.getDocumentType().getValue();
        for (DocumentReferenceType ref : docs.keySet()) {
            if (ref.getID().getValue().equals(id) && ref.getDocumentType().getValue().equals(type)) {
                return ref;
            }
        }
        return null;
    }
}