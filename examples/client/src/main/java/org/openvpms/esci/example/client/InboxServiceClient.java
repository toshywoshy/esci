/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.client;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import com.martiansoftware.jsap.Switch;
import org.openvpms.esci.service.InboxService;
import org.openvpms.esci.service.RegistryService;
import org.openvpms.esci.service.client.DefaultServiceAuthenticators;
import org.openvpms.esci.service.client.DefaultServiceLocatorFactory;
import org.openvpms.esci.service.client.ServiceLocator;
import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.basic.DocumentTypeType;
import org.openvpms.esci.ubl.common.basic.IDType;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.io.UBLDocumentWriter;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceException;
import java.net.MalformedURLException;
import java.util.Iterator;
import java.util.List;


/**
 * Example to invoke methods on the InboxService.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class InboxServiceClient {

    /**
     * The inbox service proxy.
     */
    private InboxService inbox;

    /**
     * Document marshalling context
     */
    private static UBLDocumentContext context;

    /**
     * Determines if validation should be performed.
     */
    private boolean validate;


    /**
     * Constructs an <tt>InboxServiceClient</tt>.
     *
     * @param url      the registry service WSDL url
     * @param username the user name
     * @param password the password
     * @param validate if <tt>true</tt> validate returned documents, otherwise perform no validation
     * @throws MalformedURLException if the URL is invalid
     * @throws JAXBException         for any JAXB exception
     * @throws SAXException          for any SAX exception
     * @throws WebServiceException   if a service proxy cannot be created
     */
    public InboxServiceClient(String url, String username, String password, boolean validate)
            throws MalformedURLException, JAXBException, SAXException {
        DefaultServiceLocatorFactory factory = new DefaultServiceLocatorFactory();
        factory.setServiceAuthenticators(DefaultServiceAuthenticators.getInstance());
        ServiceLocator<RegistryService> registryLocator
                = factory.getServiceLocator(RegistryService.class, url, username, password);
        RegistryService registryService = registryLocator.getService();

        String inboxURL = registryService.getInboxService();
        ServiceLocator<InboxService> inboxLocator
                = factory.getServiceLocator(InboxService.class, inboxURL, username, password);
        inbox = inboxLocator.getService();

        context = new UBLDocumentContext();
        this.validate = validate;
    }

    /**
     * Lists all available documents in the inbox.
     *
     * @throws JAXBException for any JAXB exception
     */
    public void list() throws JAXBException {
        List<DocumentReferenceType> list = inbox.getDocuments();
        System.out.println("Available documents: " + list.size());

        UBLDocumentWriter writer = context.createWriter();
        writer.setFormat(true);
        writer.setFragment(true);

        for (DocumentReferenceType ref : list) {
            writer.write(ref, System.out, validate);
        }
    }

    /**
     * Displays a document in the inbox, given its reference.
     *
     * @param ref the document reference
     * @throws JAXBException for any JAXB exception
     */
    public void print(DocumentReferenceType ref) throws JAXBException {
        Document doc = inbox.getDocument(ref);
        if (doc != null) {
            UBLDocumentWriter writer = context.createWriter();
            writer.setFormat(true);
            writer.write(doc.getAny(), System.out, validate);
        } else {
            System.out.print("Document not found");
        }
    }

    /**
     * Acknowledge a document in the inbox.
     *
     * @param ref the document reference
     * @throws DocumentNotFoundException if the document can't be found
     */
    public void acknowledge(DocumentReferenceType ref) throws DocumentNotFoundException {
        inbox.acknowledge(ref);
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     * @throws Exception for any error
     */
    public static void main(String[] args) throws Exception {
        JSAP parser = createParser();
        JSAPResult config = parser.parse(args);

        String url = config.getString("url");
        String username = config.getString("username");
        String password = config.getString("password");
        String id = config.getString("id");
        String type = config.getString("type");

        boolean list = config.getBoolean("list");
        boolean get = config.getBoolean("get");
        boolean ack = config.getBoolean("ack");
        boolean validate = !config.getBoolean("disableValidation");

        if (config.success() && (list || get || ack)) {
            InboxServiceClient client = new InboxServiceClient(url, username, password, validate);

            if (list) {
                client.list();
            } else if (id == null || type == null) {
                displayUsage(parser, config);
            } else {
                DocumentReferenceType ref = createReference(id, type);
                if (get) {
                    client.print(ref);
                } else {
                    client.acknowledge(ref);
                }
            }
        } else {
            displayUsage(parser, config);
        }
    }

    /**
     * Helper to create a document reference.
     *
     * @param id   the document identifier
     * @param type the document type
     * @return a new document reference
     */
    private static DocumentReferenceType createReference(String id, String type) {
        DocumentReferenceType ref = new DocumentReferenceType();
        IDType idType = new IDType();
        idType.setValue(id);
        DocumentTypeType docType = new DocumentTypeType();
        docType.setValue(type);
        ref.setID(idType);
        ref.setDocumentType(docType);
        return ref;
    }

    /**
     * Creates a command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new Switch("list")
                .setShortFlag('l')
                .setDefault("false")
                .setHelp("List available documents."));
        parser.registerParameter(new Switch("get")
                .setShortFlag('g')
                .setDefault("false")
                .setHelp("Retrieve a document."));
        parser.registerParameter(new Switch("ack")
                .setShortFlag('a')
                .setHelp("Acknowledge a message."));
        parser.registerParameter(new FlaggedOption("id")
                .setShortFlag('i')
                .setHelp("The document identifier."));
        parser.registerParameter(new FlaggedOption("type")
                .setShortFlag('t')
                .setHelp("The document type."));
        parser.registerParameter(new FlaggedOption("url")
                .setShortFlag('r')
                .setDefault("https://localhost:8443/esci-example/RegistryService")
                .setHelp("The RegistryService URL."));
        parser.registerParameter(new FlaggedOption("username")
                .setShortFlag('u')
                .setHelp("The service user name."));
        parser.registerParameter(new FlaggedOption("password")
                .setShortFlag('p')
                .setHelp("The service password."));
        parser.registerParameter(new Switch("disableValidation")
                .setShortFlag('d')
                .setDefault("false")
                .setHelp("Disable validation of documents"));
        return parser;
    }

    /**
     * Prints usage information and exits.
     *
     * @param parser the parser
     * @param result the parse result
     */
    private static void displayUsage(JSAP parser, JSAPResult result) {
        Iterator iter = result.getErrorMessageIterator();
        while (iter.hasNext()) {
            System.err.println(iter.next());
        }
        System.err.println();
        System.err.println("Usage: java " + InboxServiceClient.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.err.println("E.g.");
        System.err.println(". list available documents: ");
        System.err.println("  " + InboxServiceClient.class.getName() + " -l -u esci -p example");
        System.err.println(". get a document: ");
        System.err.println("  " + InboxServiceClient.class.getName() + " -g -i 267 -t Invoice -u esci -p example");
        System.err.println(". acknowledge a document: ");
        System.err.println("  " + InboxServiceClient.class.getName()
                           + " -a -i 266 -t OrderResponseSimple -u esci -p example");
        System.exit(1);
    }
}
