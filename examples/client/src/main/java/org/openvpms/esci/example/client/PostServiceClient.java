/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.example.client;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import org.openvpms.esci.example.service.PostService;
import org.openvpms.esci.service.client.DefaultServiceAuthenticators;
import org.openvpms.esci.service.client.DefaultServiceLocatorFactory;
import org.openvpms.esci.service.client.ServiceLocator;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.Iterator;


/**
 * Sends documents to the {@link PostService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class PostServiceClient {

    /**
     * The post service proxy.
     */
    private PostService service;

    /**
     * Constructs an <tt>PostServiceClient</tt>.
     *
     * @param url      the registry service WSDL url
     * @param username the user name
     * @param password the password
     * @throws MalformedURLException if the URL is invalid
     * @throws WebServiceException   if a service proxy cannot be created
     */
    public PostServiceClient(String url, String username, String password)
            throws MalformedURLException {
        DefaultServiceLocatorFactory factory = new DefaultServiceLocatorFactory();
        factory.setServiceAuthenticators(DefaultServiceAuthenticators.getInstance());
        ServiceLocator<PostService> locator = factory.getServiceLocator(PostService.class, url, username, password);

        service = locator.getService();
    }

    /**
     * Posts a document to the service.
     *
     * @param path the document path
     * @throws FileNotFoundException if the file cannot be found
     * @throws JAXBException         for any JAXB exception
     * @throws SAXException          for any SAX error
     */
    public void post(String path) throws JAXBException, SAXException, FileNotFoundException {
        UBLDocumentContext context = new UBLDocumentContext();
        Object content = context.createReader().read(new FileInputStream(path), false);
        service.post(content);
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     * @throws Exception for any error
     */
    public static void main(String[] args) throws Exception {
        JSAP parser = createParser();
        JSAPResult config = parser.parse(args);
        if (config.success()) {
            String url = config.getString("url");
            String username = config.getString("username");
            String password = config.getString("password");
            String file = config.getString("file");
            if (url != null && file != null) {
                PostServiceClient client = new PostServiceClient(url, username, password);
                client.post(file);
            } else {
                displayUsage(parser, config);
            }
        } else {
            displayUsage(parser, config);
        }
    }

    /**
     * Creates a command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("file")
                .setShortFlag('f')
                .setHelp("Post content from file."));
        parser.registerParameter(new FlaggedOption("url")
                .setShortFlag('r')
                .setDefault("https://localhost:8443/esci-example/PostService")
                .setHelp("The RegistryService URL."));
        parser.registerParameter(new FlaggedOption("username")
                .setShortFlag('u')
                .setHelp("The service user name."));
        parser.registerParameter(new FlaggedOption("password")
                .setShortFlag('p')
                .setHelp("The service password."));
        return parser;
    }

    /**
     * Prints usage information and exits.
     *
     * @param parser the parser
     * @param result the parse result
     */
    private static void displayUsage(JSAP parser, JSAPResult result) {
        Iterator iter = result.getErrorMessageIterator();
        while (iter.hasNext()) {
            System.err.println(iter.next());
        }
        System.err.println();
        System.err.println("Usage: java " + PostServiceClient.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println(parser.getHelp());
        System.exit(1);
    }
}
