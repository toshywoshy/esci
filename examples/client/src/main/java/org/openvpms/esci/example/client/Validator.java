/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.example.client;

import com.martiansoftware.jsap.FlaggedOption;
import com.martiansoftware.jsap.JSAP;
import com.martiansoftware.jsap.JSAPException;
import com.martiansoftware.jsap.JSAPResult;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.bind.JAXBException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;


/**
 * Tool to validate UBL documents against their schema.
 * <p/>
 * <em>NOTE: this does not validate that documents conform to the ESCI specification.</em>
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class Validator {

    /**
     * The UBL document context.
     */
    private UBLDocumentContext context;

    /**
     * Constructs a <tt>Validator</tt>.
     *
     * @throws JAXBException if the JAXB context cannot be created
     * @throws SAXException  if UBL document schemas cannot be read
     */
    public Validator() throws JAXBException, SAXException {
        context = new UBLDocumentContext();
    }

    /**
     * Validates a UBL document.
     *
     * @param path the file path
     * @return <tt>true</tt> if the document is valid
     */
    public boolean validate(String path) {
        boolean valid = false;
        try {
            context.createReader().read(new FileInputStream(path), true);
            valid = true;
            System.out.println("Document is valid.");
        } catch (JAXBException exception) {
            StringBuffer message = new StringBuffer("Document is invalid");
            if (exception.getLocalizedMessage() != null) {
                message.append(": ");
                message.append(exception.getLocalizedMessage());
            } else {
                message.append(".");
            }
            message.append("\n");
            Throwable cause = exception.getLinkedException();
            if (cause != null) {
                if (cause instanceof SAXParseException) {
                    SAXParseException sax = (SAXParseException) cause;
                    message.append("Line ");
                    message.append(sax.getLineNumber());
                    message.append(", column ");
                    message.append(sax.getColumnNumber());
                    message.append(": ");
                }
                message.append(cause.getLocalizedMessage());
            }
            System.err.println(message);
        } catch (FileNotFoundException exception) {
            System.err.println("File not found: " + path);
        }
        return valid;
    }

    /**
     * Main line.
     *
     * @param args command line arguments
     * @throws Exception for any error
     */
    public static void main(String[] args) throws Exception {
        JSAP parser = createParser();
        JSAPResult config = parser.parse(args);
        if (config.success()) {
            String file = config.getString("file");
            Validator validator = new Validator();
            if (validator.validate(file)) {
                System.exit(0);
            } else {
                System.exit(1);
            }
        } else {
            displayUsage(parser, config);
        }
    }

    /**
     * Creates a command line parser.
     *
     * @return a new parser
     * @throws JSAPException if the parser can't be created
     */
    private static JSAP createParser() throws JSAPException {
        JSAP parser = new JSAP();
        parser.registerParameter(new FlaggedOption("file")
                .setShortFlag('f')
                .setRequired(true)
                .setHelp("The path of the document to validate."));
        return parser;
    }

    /**
     * /**
     * Prints usage information and exits.
     *
     * @param parser the parser
     * @param result the parse result
     */
    private static void displayUsage(JSAP parser, JSAPResult result) {
        Iterator iter = result.getErrorMessageIterator();
        while (iter.hasNext()) {
            System.err.println(iter.next());
        }
        System.err.println();
        System.err.println("Usage: java " + Validator.class.getName());
        System.err.println("                " + parser.getUsage());
        System.err.println();
        System.err.println("Validates a UBL document against its XML schema.");
        System.err.println();
        System.err.println("NOTE: this does not validate that documents conform to the ESCI specification.");
        System.exit(1);
    }
}
