/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;


/**
 * Performs authentication using a group of {@link ServiceAuthenticator}s.
 * <p/>
 * The authenticator to use is selected using the supplied URL.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public interface ServiceAuthenticators extends ServiceAuthenticator {

    /**
     * Registers an authenticator for a url.
     * <p/>
     * Any existing authenticator for the url will be replaced.
     * <p/>
     * The authenticator will be registered using a weak reference.
     *
     * @param url           the url to supply credentials for
     * @param authenticator the authenticator
     */
    void addAuthenticator(String url, ServiceAuthenticator authenticator);

}
