/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.remoting.jaxws.JaxWsPortProxyFactoryBean;

import javax.jws.WebService;
import javax.xml.ws.Service;
import javax.xml.ws.WebServiceException;
import java.net.Authenticator;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.PasswordAuthentication;
import java.net.URL;


/**
 * Returns a proxy for a JAX-WS service.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
class DefaultServiceLocator<T> extends JaxWsPortProxyFactoryBean implements ServiceLocator<T> {

    /**
     * The authenticators. May be <tt>null</tt>
     */
    private ServiceAuthenticators authenticators;


    /**
     * Default constructor.
     */
    public DefaultServiceLocator() {
        this(null);
    }

    /**
     * Constructs a <tt>ServiceLocator</tt>.
     *
     * @param serviceInterface the interface of the service that this should create a proxy for. May be <tt>null</tt>
     */
    public DefaultServiceLocator(Class<T> serviceInterface) {
        if (serviceInterface != null) {
            setServiceInterface(serviceInterface);
        }
    }

    /**
     * Creates a new <tt>ServiceLocator</tt>.
     *
     * @param serviceInterface the interface of the service that this should create a proxy for. May be <tt>null</tt>
     * @return a new <tt>ServiceLocator</tt>
     */
    public static <T> DefaultServiceLocator<T> create(Class<T> serviceInterface) {
        return new DefaultServiceLocator<T>(serviceInterface);
    }

    /**
     * Creates a new <tt>ServiceLocator</tt>.
     *
     * @param serviceInterface the interface of the service that this should create a proxy for. May be <tt>null</tt>
     * @param wsdlDocumentURL  the URL of the WSDL document that describes the service. May be <tt>null</tt>
     * @return a new <tt>ServiceLocator</tt>
     * @throws MalformedURLException if the WSDL document URL is invalid
     */
    public static <T> DefaultServiceLocator<T> create(Class<T> serviceInterface, String wsdlDocumentURL)
            throws MalformedURLException {
        DefaultServiceLocator<T> result = create(serviceInterface);
        if (wsdlDocumentURL != null) {
            result.setWsdlDocumentUrl(new URL(wsdlDocumentURL));
        }
        return result;
    }

    /**
     * Creates a new <tt>ServiceLocator</tt>.
     *
     * @param serviceInterface the interface of the service that this should create a proxy for. May be <tt>null</tt>
     * @param wsdlDocumentURL  the URL of the WSDL document that describes the service. May be <tt>null</tt>
     * @param endpointAddress  the endpoint address to set on the stub. May be <tt>null</tt>
     * @return a new <tt>ServiceLocator</tt>
     * @throws MalformedURLException if the WSDL document URL is invalid
     */
    public static <T> DefaultServiceLocator<T> create(Class<T> serviceInterface, String wsdlDocumentURL,
                                                      String endpointAddress)
            throws MalformedURLException {
        DefaultServiceLocator<T> result = create(serviceInterface, wsdlDocumentURL);
        result.setEndpointAddress(endpointAddress);
        return result;
    }

    /**
     * Returns a proxy for the service.
     *
     * @return the service proxy
     * @throws WebServiceException if the service proxy cannot be created
     */
    @SuppressWarnings("unchecked")
    public T getService() {
        if (getObject() == null) {
            afterPropertiesSet();
        }
        return (T) getObject();
    }

    /**
     * Set the interface of the service that this factory should create a proxy for.
     * <p/>
     * This initialises the WSDL document URL,service name, port name and namespace URI if they are unset and
     * corresponding values have been specified by an {@link WebService} annotation on the service interface.
     */
    @Override
    public void setServiceInterface(Class serviceInterface) {
        super.setServiceInterface(serviceInterface);
        WebService annotation = getAnnotation();
        if (annotation != null) {
            if (getWsdlDocumentUrl() == null && !StringUtils.isEmpty(annotation.wsdlLocation())) {
                try {
                    setWsdlDocumentUrl(new URL(annotation.wsdlLocation()));
                } catch (MalformedURLException exception) {
                    throw new BeanInitializationException(
                            "Invalid wsdlLocation specified by " + getServiceInterface().getName() + ": "
                            + annotation.wsdlLocation(), exception);
                }
            }
            if (getServiceName() == null && !StringUtils.isEmpty(annotation.name())) {
                setServiceName(annotation.name());
            }
            if (getPortName() == null && !StringUtils.isEmpty(annotation.portName())) {
                setPortName(annotation.portName());
            }
            if (getNamespaceUri() == null && !StringUtils.isEmpty(annotation.targetNamespace())) {
                setNamespaceUri(annotation.targetNamespace());
            }
        }
    }

    /**
     * Sets the authenticators to register authenticator callbacks with.
     *
     * @param authenticators the authenticators. May be <tt>null</tt>
     */
    public void setServiceAuthenticators(ServiceAuthenticators authenticators) {
        this.authenticators = authenticators;
    }

    /**
     * Create a JAX-WS Service according to the parameters of this factory.
     *
     * @see #setServiceName
     * @see #setWsdlDocumentUrl
     */
    @Override
    public Service createJaxWsService() {
        if (authenticators != null && getWsdlDocumentUrl() != null) {
            ServiceAuthenticator authenticator = new ServiceAuthenticator() {
                @Override
                public PasswordAuthentication getPasswordAuthentication(String host, InetAddress site, int port,
                                                                        String protocol, String prompt, String scheme,
                                                                        URL url,
                                                                        Authenticator.RequestorType requestorType) {
                    char[] password = (getPassword() != null) ? getPassword().toCharArray() : new char[0];
                    return new PasswordAuthentication(getUsername(), password);
                }
            };
            authenticators.addAuthenticator(getWsdlDocumentUrl().toString(), authenticator);
        }
        return super.createJaxWsService();
    }

    /**
     * Returns the {Wlink WebService} annotation from the service interface, if one is present.
     *
     * @return the annotation, or <tt>null</tt> if none is present
     */
    @SuppressWarnings("unchecked")
    private WebService getAnnotation() {
        Class<T> serviceInterface = (Class<T>) getServiceInterface();
        return serviceInterface.getAnnotation(WebService.class);
    }

}
