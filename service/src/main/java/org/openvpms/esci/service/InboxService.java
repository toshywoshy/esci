/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.io.ObjectFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.BindingType;
import java.util.List;


/**
 * A service for retrieving documents from suppliers.
 * These may be unsolicited documents, or responses to requests.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebService(name = "InboxService", targetNamespace = "http://openvpms.org/esci")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_BINDING)
@XmlSeeAlso(ObjectFactory.class)
public interface InboxService {

    /**
     * Returns a list of document references, in the order that they were received.
     * <p/>
     * Each reference uniquely identifies a single document.
     *
     * @return a list of document references
     */
    @WebMethod
    @WebResult(name = "DocumentReference",
               targetNamespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
    List<DocumentReferenceType> getDocuments();

    /**
     * Returns the document with the specified reference.
     *
     * @param reference the document reference
     * @return the corresponding document, or <tt>null</tt> if the document is not found
     */
    @WebMethod
    @WebResult(name = "Document", targetNamespace = "http://openvpms.org/esci")
    Document getDocument(@WebParam(name = "DocumentReference",
                                   targetNamespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
                         DocumentReferenceType reference);

    /**
     * Acknowledges a document.
     * <p/>
     * Once acknowledged, the document will no longer be returned by {@link #getDocuments} nor {@link #getDocument}.
     *
     * @param reference the document reference
     * @throws DocumentNotFoundException if the reference doesn't refer to a valid document
     */
    @WebMethod
    void acknowledge(@WebParam(name = "DocumentReference",
                               targetNamespace = "urn:oasis:names:specification:ubl:schema:xsd:CommonAggregateComponents-2")
                     DocumentReferenceType reference) throws DocumentNotFoundException;

}
