/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.WebResult;
import javax.jws.soap.SOAPBinding;
import javax.xml.ws.BindingType;


/**
 * A simple registry for ESCI services.
 * <p/>
 * This enables the WSDL URLs of each ESCI service to be obtained.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebService(name = "RegistryService", targetNamespace = "http://openvpms.org/esci")
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
@BindingType(javax.xml.ws.soap.SOAPBinding.SOAP11HTTP_BINDING)
public interface RegistryService {

    /**
     * Returns the InboxService WSDL URL.
     *
     * @return the InboxService WSDL URL
     */
    @WebMethod
    @WebResult(name = "url")
    public String getInboxService();

    /**
     * Returns the OrderService WSDL URL.
     *
     * @return the OrderService WSDL URL
     */
    @WebMethod
    @WebResult(name = "url")
    public String getOrderService();

}
