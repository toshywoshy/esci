/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import java.net.Authenticator;
import java.net.InetAddress;
import java.net.PasswordAuthentication;
import java.net.URL;


/**
 * A callback that enables {@link DefaultServiceLocator} to supply authentication for services that request it.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public interface ServiceAuthenticator {

    /**
     * @param host          the host name of the site or proxy requesting authentication, or null if not available
     * @param site          the InetAddress of the site requesting authorization, or null if it's not available
     * @param port          the port number for the requested connection
     * @param protocol      the protocol that's requesting the connection.  Often this will be based on a URL, but in a
     *                      future JDK it could be, for example, "SOCKS" for a password-protected SOCKS5 firewall.
     * @param prompt        the prompt string given by the requestor (realm for http requests)
     * @param scheme        the scheme of the requestor (the HTTP scheme for an HTTP firewall, for example).
     * @param url           the URL that resulted in this request for authentication.
     * @param requestorType authentication type of the requestor
     * @return the PasswordAuthentication for the url, or <tt>null</tt> if none is available
     */
    PasswordAuthentication getPasswordAuthentication(String host, InetAddress site, int port,
                                                     String protocol, String prompt, String scheme, URL url,
                                                     Authenticator.RequestorType requestorType);

}

