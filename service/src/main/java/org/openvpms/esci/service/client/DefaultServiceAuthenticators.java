/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import org.apache.commons.collections.map.ReferenceMap;

import java.net.Authenticator;
import java.net.InetAddress;
import java.net.PasswordAuthentication;
import java.net.URL;


/**
 * Default implementation of the {@link ServiceAuthenticators} interface.
 * <p/>
 * This registers a singleton instance with {@link java.net.Authenticator#setDefault(java.net.Authenticator)}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class DefaultServiceAuthenticators extends Authenticator implements ServiceAuthenticators {

    /**
     * The authenticators.
     */
    private ReferenceMap authenticators = new ReferenceMap();

    /**
     * The singleton instance.
     */
    private static DefaultServiceAuthenticators instance;

    /**
     * Default constructor.
     */
    private DefaultServiceAuthenticators() {
    }

    /**
     * Returns the singleton instance.
     *
     * @return the singleton instance
     */
    public static synchronized DefaultServiceAuthenticators getInstance() {
        if (instance == null) {
            instance = new DefaultServiceAuthenticators();
            java.net.Authenticator.setDefault(instance);
        }
        return instance;
    }

    /**
     * Registers an authenticator for a url.
     * <p/>
     * Any existing authenticator will be replaced.
     * <p/>
     * The authenticator will be registered using a weak reference.
     *
     * @param url           the url to supply credentials for
     * @param authenticator the authenticator
     */
    public synchronized void addAuthenticator(String url, ServiceAuthenticator authenticator) {
        authenticators.put(url, authenticator);
    }


    /**
     * Called when password authorization is needed.  Subclasses should
     * override the default implementation, which returns null.
     *
     * @return The PasswordAuthentication collected from the
     *         user, or null if none is provided.
     */
    @Override
    protected synchronized PasswordAuthentication getPasswordAuthentication() {
        return getPasswordAuthentication(getRequestingHost(), getRequestingSite(), getRequestingPort(),
                                         getRequestingProtocol(), getRequestingPrompt(), getRequestingScheme(),
                                         getRequestingURL(), getRequestorType());
    }

    /**
     * @param host          the host name of the site or proxy requesting authentication, or null if not available
     * @param site          the InetAddress of the site requesting authorization, or null if it's not available
     * @param port          the port number for the requested connection
     * @param protocol      the protocol that's requesting the connection.  Often this will be based on a URL, but in a
     *                      future JDK it could be, for example, "SOCKS" for a password-protected SOCKS5 firewall.
     * @param prompt        the prompt string given by the requestor (realm for http requests)
     * @param scheme        the scheme of the requestor (the HTTP scheme for an HTTP firewall, for example).
     * @param url           the URL that resulted in this request for authentication.
     * @param requestorType authentication type of the requestor
     * @return the PasswordAuthentication for the url, or <tt>null</tt> if none is available
     */
    @Override
    public PasswordAuthentication getPasswordAuthentication(String host, InetAddress site, int port, String protocol,
                                                            String prompt, String scheme, URL url,
                                                            RequestorType requestorType) {
        if (url != null) {
            ServiceAuthenticator authenticator = (ServiceAuthenticator) authenticators.get(url.toString());
            if (authenticator != null) {
                return authenticator.getPasswordAuthentication(host, site, port, protocol, prompt, scheme, url,
                                                               requestorType);
            }
        }
        return null;
    }
}