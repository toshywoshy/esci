/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import java.net.MalformedURLException;


/**
 * Factory for {@link ServiceLocator}s.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public interface ServiceLocatorFactory {

    /**
     * Returns a <tt>ServiceLocator</tt> for the specified service interface, WSDL document URL and credentials.
     *
     * @param serviceInterface the service interface
     * @param wsdlDocumentURL  the WSDL document URL
     * @param username         the user name to connect with. May be <tt>null</tt>
     * @param password         the password to connect with. May be <tt>null</tt>
     * @return a service locator
     * @throws MalformedURLException if the WSDL document URL is invalid
     */
    <T> ServiceLocator<T> getServiceLocator(Class<T> serviceInterface, String wsdlDocumentURL,
                                            String username, String password) throws MalformedURLException;

    /**
     * Returns a <tt>ServiceLocator</tt> for the specified service interface, WSDL document URL, credentials and
     * optional endpoint address.
     *
     * @param serviceInterface the service interface
     * @param wsdlDocumentURL  the WSDL document URL
     * @param endpointAddress  the endpoint address. May be <tt>null</tt>
     * @param username         the user name to connect with. May be <tt>null</tt>
     * @param password         the password to connect with. May be <tt>null</tt>
     * @return a service locator
     * @throws MalformedURLException if the WSDL document URL is invalid
     */
    <T> ServiceLocator<T> getServiceLocator(Class<T> serviceInterface, String wsdlDocumentURL,
                                            String endpointAddress, String username, String password)
            throws MalformedURLException;

}
