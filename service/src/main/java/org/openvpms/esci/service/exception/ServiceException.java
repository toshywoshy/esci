/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.exception;


/**
 * Base class for ESCI service exceptions.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class ServiceException extends Exception {

    /**
     * Serialization id.
     */
    static final long serialVersionUID = 1;

    /**
     * Default constructor.
     */
    public ServiceException() {
    }

    /**
     * Constructs a service exception with the specified detail message.
     *
     * @param message the detail message
     */
    public ServiceException(String message) {
        super(message);
    }

    /**
     * Constructs a service exception with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause. May be <tt>null</tt>
     */
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }

}
