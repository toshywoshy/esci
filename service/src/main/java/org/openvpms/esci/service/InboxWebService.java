/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.Document;

import javax.annotation.Resource;
import javax.jws.WebService;
import java.util.List;

/**
 * Default implementation of the {@link InboxService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebService(endpointInterface = "org.openvpms.esci.service.InboxService",
            targetNamespace = "http://openvpms.org/esci", serviceName = "InboxService",
            portName = "InboxServicePort")
public class InboxWebService implements InboxService {

    /**
     * The inbox service to delegate to.
     */
    private InboxService service;


    /**
     * Returns a list of document references, in the order that they were received.
     * <p/>
     * Each reference uniquely identifies a single document.
     *
     * @return a list of document references
     */
    @Override
    public List<DocumentReferenceType> getDocuments() {
        return getInboxService().getDocuments();
    }

    /**
     * Returns the document with the specified reference.
     *
     * @param reference the document reference
     * @return the corresponding document, or <tt>null</tt> if the document is not found
     */
    @Override
    public Document getDocument(DocumentReferenceType reference) {
        return getInboxService().getDocument(reference);
    }

    /**
     * Acknowledges a document.
     * <p/>
     * Once acknowledged, the document will no longer be returned by {@link #getDocuments} nor {@link #getDocument}.
     *
     * @param reference the document reference
     * @throws DocumentNotFoundException if the reference doesn't refer to a valid document
     */
    @Override
    public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
        getInboxService().acknowledge(reference);
    }

    /**
     * Registers the inbox service to delegate to.
     *
     * @param service the inbox service
     */
    @Resource
    public void setInboxService(InboxService service) {
        this.service = service;
    }

    /**
     * Returns the inbox service.
     *
     * @return the inbox service
     */
    public InboxService getInboxService() {
        return service;
    }
}
