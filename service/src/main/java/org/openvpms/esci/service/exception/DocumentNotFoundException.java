/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service.exception;

import javax.xml.ws.WebFault;

/**
 * Exception that indicates a document was not located for a given document reference.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@WebFault(name = "DocumentNotFound", targetNamespace = "http://openvpms.org/esci")
public class DocumentNotFoundException extends ServiceException {

    /**
     * Default constructor.
     */
    public DocumentNotFoundException() {
        super();
    }

    /**
     * Constructs a <tt>DocumentNotFoundException</tt> with the specified detail message.
     *
     * @param message the detail message
     */
    public DocumentNotFoundException(String message) {
        super(message);
    }

    /**
     * Constructs a <tt>DocumentNotFoundException</tt> with the specified detail message and cause.
     *
     * @param message the detail message
     * @param cause   the cause. May be <tt>null</tt>
     */
    public DocumentNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
