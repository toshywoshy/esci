/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.service.exception.DuplicateOrderException;
import org.openvpms.esci.ubl.order.OrderType;
import org.openvpms.esci.ubl.order.Order;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * Tests the {@link OrderService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class OrderServiceTestCase extends AbstractESCITest {

    /**
     * The order service.
     */
    @Resource
    private DelegatingOrderService delegatingOrderService;


    /**
     * Verifies that a the order service can be accessed, and an order submitted.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSubmitOrder() throws Exception {
        final FutureValue<OrderType> future = new FutureValue<OrderType>();
        delegatingOrderService.setOrderService(new OrderService() {
            public void submitOrder(Order order) {
                future.set(order);
            }
        });
        OrderService proxy = getOrderService();

        Order sent = (Order) getUBL("/order.xml");
        proxy.submitOrder(sent);

        OrderType received = future.get(1000);
        assertNotNull(received);
    }

    /**
     * Verifies that {@link DuplicateOrderException} is propagated back to the client.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDuplicateOrder() throws Exception {
        final String message = "Got a duplicate";
        delegatingOrderService.setOrderService(new OrderService() {
            public void submitOrder(Order order) throws DuplicateOrderException {
                throw new DuplicateOrderException(message);
            }
        });
        OrderService proxy = getOrderService();

        Order order = (Order) getUBL("/order.xml");
        try {
            proxy.submitOrder(order);
            fail("Expected a DuplicateOrderException");
        } catch (DuplicateOrderException expected) {
            assertEquals(expected.getMessage(), message);
        } catch (Throwable exception) {
            fail("Expected a DuplicateOrderException but got " + exception);
        }
    }

    /**
     * Returns a proxy to the order service.
     *
     * @return a proxy to the order service
     * @throws IOException for any I/O error
     */
    private OrderService getOrderService() throws IOException {
        return getService(OrderService.class, "/wsdl/OrderService.wsdl", "orderService");
    }

}
