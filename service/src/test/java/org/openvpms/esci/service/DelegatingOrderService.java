/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service;

import org.openvpms.esci.service.exception.DuplicateOrderException;
import org.openvpms.esci.ubl.order.Order;


/**
 * Implementation of the {@link OrderService} that delegates to another.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class DelegatingOrderService implements OrderService {

    /**
     * The service to delegate to.
     */
    private OrderService service;

    /**
     * Registers the service to delegate to.
     *
     * @param service the service
     */
    public void setOrderService(OrderService service) {
        this.service = service;
    }

    /**
     * Submits an order.
     *
     * @param order the order to submit
     * @throws DuplicateOrderException if the order has a duplicate identifier to one already submitted
     */
    @Override
    public void submitOrder(Order order) throws DuplicateOrderException {
        service.submitOrder(order);
    }
}
