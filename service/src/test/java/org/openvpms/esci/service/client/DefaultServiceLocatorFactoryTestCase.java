/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.service.client;

import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.service.AbstractESCITest;
import org.openvpms.esci.service.DelegatingOrderService;
import org.openvpms.esci.service.OrderService;
import org.openvpms.esci.ubl.order.OrderType;
import org.openvpms.esci.ubl.order.Order;

import javax.annotation.Resource;

/**
 * Tests the {@link org.openvpms.esci.service.client.DefaultServiceLocatorFactory} class.
 * <p/>
 * This uses the "in-vm" transport.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class DefaultServiceLocatorFactoryTestCase extends AbstractESCITest {

    /**
     * The order service.
     */
    @Resource
    private DelegatingOrderService delegatingOrderService;

    /**
     * Verifies that a proxy for the order service can be obtained, and its methods invoked.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGetProxy() throws Exception {
        applicationContext.getBean("orderService"); // force registration of the order service.
        final FutureValue<OrderType> future = new FutureValue<OrderType>();
        ServiceLocatorFactory factory = new DefaultServiceLocatorFactory();
        ServiceLocator<OrderService> locator = factory.getServiceLocator(OrderService.class,
                                                                         getWSDL("wsdl/OrderService.wsdl"),
                                                                         "in-vm://orderService/", null, null);
        delegatingOrderService.setOrderService(new OrderService() {
            public void submitOrder(Order order) {
                future.set(order);
            }
        });
        OrderService proxy = locator.getService();
        Order sent = (Order) getUBL("/order.xml");
        proxy.submitOrder(sent);
        OrderType received = future.get(1000);
        assertNotNull(received);
    }


}
