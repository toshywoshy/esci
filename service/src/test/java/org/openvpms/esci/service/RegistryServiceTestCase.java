/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import javax.annotation.Resource;
import java.io.IOException;


/**
 * Tests the {@link RegistryService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class RegistryServiceTestCase extends AbstractESCITest {

    /**
     * The registry service.
     */
    @Resource
    private DelegatingRegistryService delegatingRegistryService;


    /**
     * Verifies that a the order service can be accessed, and an order submitted.
     *
     * @throws Exception for any error
     */
    @Test
    public void testSubmitOrder() throws Exception {
        final String inbox = "http://foo/inbox";
        final String order = "http://foo/orders";

        delegatingRegistryService.setRegistry(new RegistryService() {
            @Override
            public String getInboxService() {
                return inbox;
            }

            @Override
            public String getOrderService() {
                return order;
            }
        });
        RegistryService proxy = getRegistryService();

        assertEquals(inbox, proxy.getInboxService());
        assertEquals(order, proxy.getOrderService());
    }

    /**
     * Returns a proxy to the registry service.
     *
     * @return a proxy to the registry service
     * @throws IOException for any I/O error
     */
    private RegistryService getRegistryService() throws IOException {
        return getService(RegistryService.class, "/wsdl/RegistryService.wsdl", "registryService");
    }

}