/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.openvpms.esci.FutureValue;
import org.openvpms.esci.service.exception.DocumentNotFoundException;
import org.openvpms.esci.ubl.common.Document;
import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;
import org.openvpms.esci.ubl.common.basic.IDType;
import org.openvpms.esci.ubl.order.OrderResponseSimpleType;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Tests the {@link InboxService}.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
public class InboxServiceTestCase extends AbstractESCITest {

    /**
     * The inbox service.
     */
    @Resource
    private DelegatingInboxService delegatingInboxService;

    /**
     * Identifier seed.
     */
    private int seed;


    /**
     * Verifies that a reference to the inbox service can be obtained, and its methods invoked.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGetDocuments() throws Exception {
        DocumentReferenceType doc1 = createDocumentReference();
        DocumentReferenceType doc2 = createDocumentReference();
        final List<DocumentReferenceType> docs = new ArrayList<DocumentReferenceType>();
        docs.add(doc1);
        docs.add(doc2);

        delegatingInboxService.setInboxService(new TestInboxService() {
            @Override
            public List<DocumentReferenceType> getDocuments() {
                return docs;
            }
        });
        InboxService proxy = getInboxService();

        // invoke getDocuments() and verify the the results match that expected
        List<DocumentReferenceType> resultDocs = proxy.getDocuments();
        assertFalse(docs == resultDocs); // should be a new instance
        assertEquals(docs.size(), resultDocs.size());
        assertEquals(resultDocs.get(0).getID().getValue(), resultDocs.get(0).getID().getValue());
        assertEquals(resultDocs.get(1).getID().getValue(), resultDocs.get(1).getID().getValue());
    }

    /**
     * Tests the {@link InboxService#getDocument} method.
     *
     * @throws Exception for any error
     */
    @Test
    public void testGetDocument() throws Exception {
        final OrderResponseSimpleType response = (OrderResponseSimpleType) getUBL("/response.xml");

        delegatingInboxService.setInboxService(new TestInboxService() {
            @Override
            public Document getDocument(DocumentReferenceType reference) {
                Document result = new Document();
                result.setAny(response);
                return result;
            }
        });
        InboxService proxy = getInboxService();

        Document doc = proxy.getDocument(createDocumentReference());
        assertNotNull(doc);
        Object any = doc.getAny();
        assertNotNull(any);
        assertFalse(any == response); // should be a new instance
        assertEquals(((OrderResponseSimpleType) any).getID().getValue(), response.getID().getValue());
    }

    /**
     * Tests the {@link InboxService#acknowledge} method.
     *
     * @throws Exception for any error
     */
    @Test
    public void testAcknowledge() throws Exception {
        final FutureValue<DocumentReferenceType> future = new FutureValue<DocumentReferenceType>();
        delegatingInboxService.setInboxService(new TestInboxService() {
            @Override
            public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
                future.set(reference);
            }
        });

        InboxService proxy = getInboxService();
        DocumentReferenceType sent = createDocumentReference();
        proxy.acknowledge(sent);

        DocumentReferenceType received = future.get(1000);
        assertNotNull(received);
        assertFalse(received == sent);
        assertEquals(sent.getID().getValue(), received.getID().getValue());
    }

    /**
     * Verifies that {@link DocumentNotFoundException} is propagated back to the client.
     *
     * @throws Exception for any error
     */
    @Test
    public void testDocumentNotFound() throws Exception {
        final String message = "Got a duplicate";
        delegatingInboxService.setInboxService(new TestInboxService() {
            public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
                throw new DocumentNotFoundException(message);
            }
        });
        InboxService proxy = getInboxService();

        try {
            proxy.acknowledge(createDocumentReference());
            fail("Expected a DocumentNotFoundException");
        } catch (DocumentNotFoundException expected) {
            assertEquals(expected.getMessage(), message);
        } catch (Throwable exception) {
            fail("Expected a DocumentNotFoundException but got " + exception);
        }
    }

    /**
     * Returns a proxy to the inbox service.
     *
     * @return a proxy to the inbox service
     * @throws IOException for any I/O error
     */
    private InboxService getInboxService() throws IOException {
        return getService(InboxService.class, "/wsdl/InboxService.wsdl", "inboxService");
    }

    /**
     * Helper to create a document reference with an unique id.
     *
     * @return a new document reference
     */
    private DocumentReferenceType createDocumentReference() {
        DocumentReferenceType result = new DocumentReferenceType();
        IDType id = new IDType();
        id.setValue(Long.toString(++seed));
        result.setID(id);
        return result;
    }


    private static class TestInboxService implements InboxService {

        @Override
        public List<DocumentReferenceType> getDocuments() {
            return null;
        }

        @Override
        public Document getDocument(DocumentReferenceType reference) {
            return null;
        }

        @Override
        public void acknowledge(DocumentReferenceType reference) throws DocumentNotFoundException {
        }
    }

}