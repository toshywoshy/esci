/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.service;

import org.openvpms.esci.service.client.DefaultServiceLocatorFactory;
import org.openvpms.esci.service.client.ServiceLocator;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractJUnit4SpringContextTests;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import javax.xml.ws.WebServiceException;
import java.io.IOException;


/**
 * Base class for ESCI test cases.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@ContextConfiguration("/applicationContext.xml")
public abstract class AbstractESCITest extends AbstractJUnit4SpringContextTests {

    /**
     * The document context.
     */
    private UBLDocumentContext context;


    /**
     * Helper to return an object representing an UBL document.
     *
     * @param resourcePath the UBL document resource path
     * @return an object representing the document
     * @throws JAXBException if the document cannot be deserialized or the JAXB context cannot be created
     * @throws SAXException  if UBL document schemas cannot be read
     */
    protected Object getUBL(String resourcePath) throws JAXBException, SAXException {
        if (context == null) {
            context = new UBLDocumentContext();
        }
        return context.createReader().read(getClass().getResourceAsStream(resourcePath));
    }

    /**
     * Returns a proxy for a service residing in the current vm.
     *
     * @param sei      the service endpoint interface
     * @param wsdl     the WSDL resource path
     * @param beanName the bean name
     * @return a proxy for the service
     * @throws IOException         for any I/O error
     * @throws WebServiceException if the service cannot be created
     */
    protected <T> T getService(Class<T> sei, String wsdl, String beanName) throws IOException {
        applicationContext.getBean(beanName); // force registration of the service
        DefaultServiceLocatorFactory factory = new DefaultServiceLocatorFactory();
        ServiceLocator<T> locator = factory.getServiceLocator(sei, getWSDL(wsdl), "in-vm://" + beanName, null, null);
        return locator.getService();
    }

    /**
     * Helper to return the URL of a WSDL file, given its resource path.
     *
     * @param resourcePath the path to the WSDL resource
     * @return the URL of the WSDL resource
     * @throws IOException if the URL is invalid
     */
    protected String getWSDL(String resourcePath) throws IOException {
        ClassPathResource wsdl = new ClassPathResource(resourcePath);
        return wsdl.getURL().toString();
    }

}
