/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci;

/**
 * Helper encapsulating a value that is set some time in the future.
 * <p/>
 * Callers can block waiting for the value to be set.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class FutureValue<T> {

    /**
     * The value.
     */
    private volatile T value;

    /**
     * Sets the value.
     *
     * @param value the value
     */
    public void set(T value) {
        this.value = value;
        synchronized (this) {
            notifyAll();
        }
    }

    /**
     * Retrieves the value.
     *
     * @param timeout the maximum time in milliseconds to wait.
     * @return the value, or <tt>null</tt> if it was not set
     * @throws InterruptedException if the thread is interrupted
     */
    public T get(long timeout) throws InterruptedException {
        if (value == null) {
            synchronized (this) {
                wait(timeout);
            }
        }
        return value;
    }

}
