/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl.io;

import org.openvpms.esci.ubl.common.aggregate.DocumentReferenceType;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.OutputStream;


/**
 * UBL Document writer.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class UBLDocumentWriter {

    /**
     * The document context.
     */
    private final UBLDocumentContext context;

    /**
     * Determines if the output should be formatted.
     */
    private boolean format;

    /**
     * Determines if written elements are part of a document. If so, document level information is surpressed.
     */
    private boolean fragment;

    /**
     * The common aggregate component factory.
     */
    private static final org.openvpms.esci.ubl.common.aggregate.ObjectFactory cacFactory
            = new org.openvpms.esci.ubl.common.aggregate.ObjectFactory();


    /**
     * Constructs a <tt>UBLDocumentWriter</tt>.
     *
     * @param context the context
     */
    public UBLDocumentWriter(UBLDocumentContext context) {
        this.context = context;
    }

    /**
     * Determines if the output should be formatted.
     *
     * @param format if <tt>true</tt>, format the output
     */
    public void setFormat(boolean format) {
        this.format = format;
    }

    /**
     * Determines if the object is a fragment of a document.
     * If so, document level declarations are omitted. Defaults to <tt>false</tt>
     *
     * @param fragment if <tt>true</tt>, suppress document level declarations
     */
    public void setFragment(boolean fragment) {
        this.fragment = fragment;
    }

    /**
     * Writes an UBL object to a stream.
     * <p/>
     * Validation will be performed.
     *
     * @param object the object to write
     * @param stream the stream to write to
     * @throws JAXBException            for any error
     * @throws IllegalArgumentException if <tt>object</tt> is not a supported type
     */
    public void write(Object object, OutputStream stream) throws JAXBException {
        write(object, stream, true);
    }

    /**
     * Writes an UBL object to a stream, optionally performing validation.
     *
     * @param object   the object to write
     * @param stream   the stream to write to
     * @param validate if <tt>true</tt> validate the object against it's schema
     * @throws JAXBException            for any error
     * @throws IllegalArgumentException if <tt>object</tt> is not a supported type
     */
    public void write(Object object, OutputStream stream, boolean validate) throws JAXBException {
        if (object instanceof DocumentReferenceType) {
            write((DocumentReferenceType) object, stream, validate);
        } else {
            marshal(object, stream, validate);
        }
    }

    /**
     * Writes a document reference to a stream.
     * <p/>
     * Validation will be performed.
     *
     * @param reference the reference to write
     * @param stream    the stream to write to
     * @throws JAXBException for any error
     */
    public void write(DocumentReferenceType reference, OutputStream stream) throws JAXBException {
        write(reference, stream, true);
    }

    /**
     * Writes a simple order response to a stream, optionally validating it.
     *
     * @param reference the simple order response to write
     * @param stream    the stream to write to
     * @param validate  if <tt>true</tt> validate the order against it's schema
     * @throws JAXBException for any error
     */
    public void write(DocumentReferenceType reference, OutputStream stream, boolean validate) throws JAXBException {
        JAXBElement<DocumentReferenceType> element = cacFactory.createDocumentReference(reference);
        marshal(element, stream, validate);
    }

    /**
     * Writes an object to a stream, optionally validating it.
     *
     * @param object   the object to write
     * @param stream   the stream to write to
     * @param validate if <tt>true</tt> validate the order against it's schema
     * @throws JAXBException for any error
     */
    private void marshal(Object object, OutputStream stream, boolean validate) throws JAXBException {
        Marshaller marshaller = context.getContext().createMarshaller();
        if (validate) {
            marshaller.setSchema(context.getSchema());
        }
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, format);
        marshaller.setProperty(Marshaller.JAXB_FRAGMENT, fragment);
        marshaller.marshal(object, stream);
    }

}
