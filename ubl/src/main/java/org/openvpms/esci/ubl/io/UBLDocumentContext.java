/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl.io;

import org.xml.sax.SAXException;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.net.URL;


/**
 * Helper for reading and writing UBL documents.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class UBLDocumentContext {

    /**
     * The JAXB context
     */
    private final JAXBContext context;

    /**
     * The UBL schema.
     */
    private Schema schema;

    /**
     * Order schema resource path.
     */
    private static final String ORDER_SCHEMA = "/os-UBL-2.0/maindoc/UBL-Order-2.0.xsd";

    /**
     * Order response simple schema resource path.
     */
    private static final String ORDER_RESPONSE_SIMPLE_SCHEMA = "/os-UBL-2.0/maindoc/UBL-OrderResponseSimple-2.0.xsd";

    /**
     * Invoice schema resource path.
     */
    private static final String INVOICE_SCHEMA = "/os-UBL-2.0/maindoc/UBL-Invoice-2.0.xsd";

    /**
     * The schemas.
     */
    private static String[] SCHEMAS = {ORDER_SCHEMA, ORDER_RESPONSE_SIMPLE_SCHEMA, INVOICE_SCHEMA};


    /**
     * Constructs a <tt>UBLDocumentContext</tt>.                    I
     *
     * @throws JAXBException         if the JAXB context cannot be created
     * @throws SAXException          if UBL document schemas cannot be read
     * @throws IllegalStateException if a schema resource doesn't exist
     */
    public UBLDocumentContext() throws JAXBException, SAXException {
        context = JAXBContext.newInstance("org.openvpms.esci.ubl.io");
        SchemaFactory sf = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        Source[] schemas = new Source[SCHEMAS.length];
        for (int i = 0; i < schemas.length; ++i) {
            schemas[i] = getSchema(SCHEMAS[i]);
        }
        schema = sf.newSchema(schemas);
    }

    /**
     * Creates a new UBL document reader.
     *
     * @return a new reader
     */
    public UBLDocumentReader createReader() {
        return new UBLDocumentReader(this);
    }

    /**
     * Creates a new UBL document writer.
     *
     * @return a new writer
     */
    public UBLDocumentWriter createWriter() {
        return new UBLDocumentWriter(this);
    }

    /**
     * Returns the UBL schema.
     *
     * @return the UBL schema
     */
    public Schema getSchema() {
        return schema;
    }

    /**
     * Returns the JAXB context.
     *
     * @return the JAXB context
     */
    protected JAXBContext getContext() {
        return context;
    }

    /**
     * Returns a schema source given its resource path.
     *
     * @param path the schema resource path
     * @return the schema read from the resource path
     * @throws IllegalStateException if the schema resource doesn't exist
     */
    private Source getSchema(String path) {
        URL schemaURL = UBLDocumentContext.class.getResource(path);
        if (schemaURL == null) {
            throw new IllegalStateException("Schema not found: " + path);
        }
        return new StreamSource(schemaURL.toExternalForm());
    }

}
