/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2011 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id: $
 */

package org.openvpms.esci.ubl.io;

import org.openvpms.esci.ubl.invoice.Invoice;
import org.openvpms.esci.ubl.order.Order;
import org.openvpms.esci.ubl.order.OrderResponseSimple;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This contains factory methods for customisations of the UBL complex types, in order for them to marshal corrrectly.
 * <p/>
 * This is indirectly used by the {@link UBLDocumentContext} class.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: $
 */
@XmlRegistry
public class ObjectFactory {

    /**
     * Default constructor.
     */
    public ObjectFactory() {
    }

    /**
     * Creates an {@link Order}.
     *
     * @return a new order
     */
    public Order createOrder() {
        return new Order();
    }

    /**
     * Creates an {@link Invoice}.
     *
     * @return a new invoice
     */
    public Invoice createInvoice() {
        return new Invoice();
    }

    /**
     * Creates an {@link OrderResponseSimple}.
     *
     * @return a new order response
     */
    public OrderResponseSimple createOrderResponseSimple() {
        return new OrderResponseSimple();
    }

}
