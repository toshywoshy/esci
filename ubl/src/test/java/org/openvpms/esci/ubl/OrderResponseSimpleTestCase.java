/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;
import org.openvpms.esci.ubl.common.aggregate.CustomerPartyType;
import org.openvpms.esci.ubl.common.aggregate.SupplierPartyType;
import org.openvpms.esci.ubl.io.UBLDocumentContext;
import org.openvpms.esci.ubl.order.OrderResponseSimpleType;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;


/**
 * Tests unmarshalling and marshalling of simple order responses.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public class OrderResponseSimpleTestCase extends MarshallingTestCase {

    /**
     * Unmarshals a simple order response from a file, marshals it to memory, and unmarshals again.
     * The final unmarshalled object is verified against the expected results.
     *
     * @throws Exception for any error
     */
    @Test
    public void testRoundtrip() throws Exception {
        UBLDocumentContext context = new UBLDocumentContext();

        // read a valid order response from a file
        InputStream stream = OrderResponseSimpleTestCase.class.getResourceAsStream("/orderResponseSimple.xml");
        assertNotNull(stream);
        OrderResponseSimpleType response = (OrderResponseSimpleType) context.createReader().read(stream);

        // write it to a byte stream
        ByteArrayOutputStream ostream = new ByteArrayOutputStream();
        context.createWriter().write(response, ostream);

        // read the order response from the byte stream
        ByteArrayInputStream istream = new ByteArrayInputStream(ostream.toByteArray());
        response = (OrderResponseSimpleType) context.createReader().read(istream);

        // now verify the order response has the expected content
        checkId("2.0", response.getUBLVersionID());
        assertEquals(false, response.getCopyIndicator().isValue());
        checkId("569ED478-0EBE-4817-A234-DFB9ACA81218", response.getUUID());
        assertEquals("2005-06-20", response.getIssueDate().getValue().toString());
        assertEquals(1, response.getNote().size());
        assertEquals("sample", response.getNote().get(0).getValue());
        CustomerPartyType customer = response.getBuyerCustomerParty();
        assertEquals("CV12345", customer.getSupplierAssignedAccountID().getValue());
        checkParty(customer.getParty(), "Phillip Island Veterinary Clinic", "42", "Phillip Island Rd",
                   "New Haven", "3925", "VIC");

        SupplierPartyType supplier = response.getSellerSupplierParty();
        assertEquals("1234", supplier.getCustomerAssignedAccountID().getValue());
        checkParty(supplier.getParty(), "Cenvet Pty Ltd", "26", "Binney Road", "Kings Park", "2148", "NSW");
    }


}