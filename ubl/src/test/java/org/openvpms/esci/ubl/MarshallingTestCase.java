/*
 *  Version: 1.0
 *
 *  The contents of this file are subject to the OpenVPMS License Version
 *  1.0 (the 'License'); you may not use this file except in compliance with
 *  the License. You may obtain a copy of the License at
 *  http://www.openvpms.org/license/
 *
 *  Software distributed under the License is distributed on an 'AS IS' basis,
 *  WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 *  for the specific language governing rights and limitations under the
 *  License.
 *
 *  Copyright 2010 (C) OpenVPMS Ltd. All Rights Reserved.
 *
 *  $Id$
 */
package org.openvpms.esci.ubl;

import static org.junit.Assert.assertEquals;
import org.openvpms.esci.ubl.common.AmountType;
import org.openvpms.esci.ubl.common.IdentifierType;
import org.openvpms.esci.ubl.common.QuantityType;
import org.openvpms.esci.ubl.common.aggregate.AddressType;
import org.openvpms.esci.ubl.common.aggregate.ItemType;
import org.openvpms.esci.ubl.common.aggregate.PartyType;
import org.openvpms.esci.ubl.common.aggregate.TaxTotalType;

import java.util.List;

/**
 * Base class for marshalling tests.
 *
 * @author <a href="mailto:support@openvpms.org">OpenVPMS Team</a>
 * @version $LastChangedDate: 2006-05-02 05:16:31Z $
 */
public abstract class MarshallingTestCase {

    /**
     * Checks a party.
     *
     * @param party                the party to check
     * @param name                 the expected party name
     * @param buildingNumber       the expected address building no.
     * @param streetName           the expected address street name
     * @param cityName             the expected address city name
     * @param postalZone           the expected address postal zone
     * @param countrySubentityCode the expected address country sub entity code
     */
    protected void checkParty(PartyType party, String name, String buildingNumber, String streetName,
                              String cityName, String postalZone, String countrySubentityCode) {
        assertEquals(1, party.getPartyName().size());
        assertEquals(name, party.getPartyName().get(0).getName().getValue());

        AddressType address = party.getPostalAddress();
        assertEquals(buildingNumber, address.getBuildingNumber().getValue());
        assertEquals(streetName, address.getStreetName().getValue());
        assertEquals(cityName, address.getCityName().getValue());
        assertEquals(postalZone, address.getPostalZone().getValue());
        assertEquals(countrySubentityCode, address.getCountrySubentityCode().getValue());
    }

    /**
     * Verifies that the tax matches that expected.
     *
     * @param expected the expected tax
     * @param tax      the tax list (must contain 1 element)
     */
    protected void checkTaxTotal(String expected, List<TaxTotalType> tax) {
        assertEquals(1, tax.size());
        checkAmount(expected, tax.get(0).getTaxAmount());
    }

    /**
     * Verifies an amount matches that expected.
     *
     * @param expected the expected value
     * @param actual   the actual value
     */
    protected void checkAmount(String expected, AmountType actual) {
        assertEquals(expected, actual.getValue().toString());
        assertEquals("AUD", actual.getCurrencyID().value());
    }

    /**
     * Verifies an id matches that expected.
     *
     * @param expected the expected value
     * @param actual   the actual value
     */
    protected void checkId(String expected, IdentifierType actual) {
        assertEquals(expected, actual.getValue());
    }

    /**
     * Checks a quanity.
     *
     * @param quantity the quantity to check
     * @param code     the expected code
     * @param value    the expected value
     */
    protected void checkQuantity(QuantityType quantity, String code, int value) {
        assertEquals(code, quantity.getUnitCode());
        assertEquals(value, quantity.getValue().toBigInteger().intValue());
    }

    /**
     * Checks an item.
     *
     * @param item        the item to check
     * @param name        the expected item name
     * @param description the expected item description
     * @param buyerId     the expected buyer identifier
     * @param sellerId    the expected seller identifier
     */
    protected void checkItem(ItemType item, String name, String description, String buyerId, String sellerId) {
        assertEquals(name, item.getName().getValue());
        assertEquals(1, item.getDescription().size());
        assertEquals(description, item.getDescription().get(0).getValue());
        assertEquals(buyerId, item.getBuyersItemIdentification().getID().getValue());
        assertEquals(sellerId, item.getSellersItemIdentification().getID().getValue());
    }
}
